import {CHANGE_INPUT,ADD_ITEM,DELETE_ITEM,GET_LIST} from './actionTypes'

const defaultState = {
    inputValue : 'Write Something',
    list:[
        '工作再忙也要锻炼身体',
        '中午下班休息一小时'
    ]
}

const stateAction = (state = defaultState,action)=>{

    if(action.type === CHANGE_INPUT){
        let newState = JSON.parse(JSON.stringify(state)) //深度拷贝state
        newState.inputValue = action.value
        return newState
    }

    //state值只能传递，不能使用
    if(action.type === ADD_ITEM ){
        //根据type值，编写业务逻辑
        let newState = JSON.parse(JSON.stringify(state))
        //push新的内容到列表中去
        newState.list.push(newState.inputValue)
        newState.inputValue = ''
        return newState
    }

    //根据type值，编写业务逻辑
    if(action.type === DELETE_ITEM ){ 
        let newState = JSON.parse(JSON.stringify(state))
        //push新的内容到列表中去
        newState.list.splice(action.index,1)
        return newState
    }

    if(action.type === GET_LIST ){ //根据type值，编写业务逻辑
        let newState = JSON.parse(JSON.stringify(state)) 
        newState.list = action.data.data.list //复制性的List数组进去
        return newState
    }

    return state
}

export default stateAction;